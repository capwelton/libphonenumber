; <?php
/*
[general]
name="libphonenumber"
version="0.1.2"
addon_type="LIBRARY"
mysql_character_set_database="latin1,utf8"
encoding="UTF-8"
description="Phone number manipulation library."
description.fr="Librairie pour manipulation des numéros de téléphone"
delete=1
longdesc=""
ov_version="8.0.0"
php_version="5.3.0"
addon_access_control=0
author="Robin Bailleux (robin.bailleux@si-4you.com)"
icon="icon.png"


;*/
?>
